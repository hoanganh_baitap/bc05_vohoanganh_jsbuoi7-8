// THÊM SỐ VÀO MẢNG KHI NGƯỜI DÙNG CLICK VÀO THÊM SỐ
// tạo mảng chứa những số dc người dùng thêm vào
var arrNum = [];
// gắn sự kiện khi người dùng click vào ô thêm số
function themSo() {
  // input: number
  var number = document.getElementById("txt-num").value * 1;
  // reset ô nput khi người dùng thêm số xong
  document.getElementById("txt-num").value = "";
  // thêm số vào mảng
  arrNum.push(number);
  // show ra giao diện
  document.getElementById("themSo").innerText = arrNum;
}

// BÀI 1: TÍNH TỔNG SỐ DƯƠNG TRONG MẢNG
function tinhTongDuong() {
  // input: arrNum
  // output: tổng số dương
  var tongDuong = 0;
  // duyệt qua mảng
  for (var i = 0; i < arrNum.length; i++) {
    if (arrNum[i] > 0) {
      tongDuong += arrNum[i];
    }
  }
  // show ra giao diện
  document.getElementById("result1").innerText = `Tổng số dương = ${tongDuong}`;
}

// BÀI 2: ĐẾM SỐ DƯƠNG CÓ TRONG MẢNG
function demSoDuong() {
  // input: arrNum
  // output: đếm số dương
  var demSoDuong = 0;
  // duyệt mảng
  for (var i = 0; i < arrNum.length; i++) {
    if (arrNum[i] > 0) {
      demSoDuong += 1;
    }
  }
  // show ra giao diện
  document.getElementById("result2").innerText = `Có ${demSoDuong} số dương`;
}

// BÀI 3: TÌM SỐ NHỎ NHẤT
function soNhoNhat() {
  // input: arrNum
  // output: số nhỏ nhất
  var soNhoNhat = [0];
  // duyệt mảng
  for (var i = 0; i < arrNum.length; i++) {
    if (arrNum[i] < soNhoNhat) {
      soNhoNhat = arrNum[i];
    }
  }
  // show ra giao diện
  document.getElementById("result3").innerText = `Số nhỏ nhất là ${soNhoNhat}`;
}

// BÀI 4: TÌM SỐ DƯƠNG NHỎ NHẤT
function soDuongNhoNhat() {
  // input: arrNum
  // output: số dương nhỏ nhất
  var soDuongNhoNhat = [0];
  // duyệt mảng tìm ra số dương
  var soDuong = 0;
  for (var i = 0; i < arrNum.length; i++) {
    if (arrNum[i] > 0) {
      soDuong = arrNum[i];
    }
    if (soDuong < soDuongNhoNhat || soDuongNhoNhat == 0) {
      soDuongNhoNhat = soDuong;
    }
  }
  // show ra giao diện
  document.getElementById(
    "result4"
  ).innerText = `Số dương nhỏ nhất là ${soDuongNhoNhat}`;
}

// BÀI 5: TÌM SỐ CHẴN CUỐI TRONG MẢNG
function soChanCuoi() {
  // input: arrNum
  // output: số chẵn cuối
  var soChanCuoi = "";
  // duyệt mảng
  for (var i = 0; i < arrNum.length; i++) {
    if (arrNum[i] % 2 == 0 && arrNum[i] > 0) {
      soChanCuoi = arrNum[i];
    }
  }
  // show ra giao diện
  document.getElementById("result5").innerText = soChanCuoi;
}

// BÀI 6: ĐỔI VỊ TRÍ
function doiViTri() {
  // input: arrNum, vị trí 1, vị trí 2
  var viTri1 = document.getElementById("vi-tri-1").value * 1;
  var viTri2 = document.getElementById("vi-tri-2").value * 1;
  arrNum[viTri1];
  arrNum[viTri2];
  // tạo biến tạm
  var bienTam = arrNum[viTri1];
  arrNum[viTri1] = arrNum[viTri2];
  arrNum[viTri2] = bienTam;
  // show ra giao diện
  document.getElementById("result6").innerText = arrNum;
}

// BÀI 7: SẮP XẾP TĂNG DẦN
function sapXep() {
  // input: arrNum
  // output: sắp xếp tăng
  // hàm sửa lỗi do sort gây nên
  function sortNumber(a, b) {
    return a - b;
  }
  // sort
  arrNum.sort(sortNumber);
  // show ra giao diện
  document.getElementById("result7").innerText = arrNum;
}

// BÀI 8: TÌM SỐ NGUYÊN TỐ ĐẦU TIÊN
// hàm tìm số nguyên tố
function laSoNguyenTo(n) {
  if (n < 2) {
    return false;
  }
  if (n == 2) {
    return true;
  }
  if (n % 2 == 0) {
    return false;
  }
  for (var i = 3; i < n - 1; i += 2) {
    if (n % i == 0) {
      return false;
    }
    return true;
  }
}
function soNguyenTo() {
  // input: arrNum
  // output: số nguyên tố đầu tiên
  var soNguyenToDauTien = "";
  // duyệt qua mảng
  for (var i = 0; i < arrNum.length; i++) {
    if (laSoNguyenTo(arrNum[i]) == true) {
      soNguyenToDauTien = arrNum[i];
      break;
    } else {
      soNguyenToDauTien = "Không có số nguyên tố trong mảng";
    }
  }
  // show ra giao diện
  document.getElementById("result8").innerText = soNguyenToDauTien;
}

// BÀI 9: TÌM SỐ NGUYÊN CÓ TRONG MẢNG
function soNguyen() {
  // input: arrNum
  // output: mảng chứa số nguyên
  var arrSoNguyen = [];
  // duyệt mảng
  for (var i = 0; i < arrNum.length; i++) {
    // kiểm tra là số nguyên tố
    if (Number.isInteger(arrNum[i])) {
      // push số nguyên dc kiểm tra vào mảng số nguyên
      arrSoNguyen.push(arrNum[i]);
    }
  }
  // show ra giao diện
  document.getElementById("result9").innerText = arrSoNguyen;
}

// BÀI 10: SO SÁNH SỐ LƯỢNG SỐ ÂM, DƯƠNG TRONG MẢNG
function soSanh() {
  // input: arrNum
  // output:so sánh số âm, số dương
  var soSanh = "";
  var soAm = 0;
  var soDuong = 0;
  // duyệt mảng
  for (var i = 0; i < arrNum.length; i++) {
    if (arrNum[i] > 0) {
      soDuong += 1;
    } else {
      soAm += 1;
    }
    if (soAm > soDuong) {
      soSanh = `Có ${soAm} số âm > ${soDuong} số dương`;
    } else {
      soSanh = `Có ${soAm} số âm < ${soDuong} số dương`;
    }
  }
  // show ra giao diện
  document.getElementById("result10").innerText = soSanh;
}
